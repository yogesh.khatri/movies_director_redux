import React, { Component } from 'react'
import { Link, withRouter } from "react-router-dom"
import { deleteDirector, getData } from './actions/index'
import { connect } from 'react-redux'


class DirectorDiv extends Component {

    // object to key-value pair list
    ObjectToList(director) {
        let ListOfDirectorsDetails = []
        for (let property in director) {
            let temp = [];
            temp.push(property);
            temp.push(director[property]);
            ListOfDirectorsDetails.push(temp)
        }
        return ListOfDirectorsDetails;
    }

    // on delete button pressed
    deleteBtn = async (e) => {
        console.log(this.props.director.id)
        await this.props.deleteDirector(this.props.director.id)
        this.props.getData()
        this.props.history.push('/directors')
    }


    render() {
        return (
            <div
                style={{
                    bordergscgscStyle: "solid",
                    backgroundColor: "beige",
                    width: "40%",
                    margin: "3%",
                    padding: "1%"
                }}>
                {
                    this.ObjectToList(this.props.director).map((property) => {
                        return (
                            <DirectorDivItem
                                property={property[0]}
                                value={property[1]}
                                key={property[0]}
                            />)
                    })
                }
                {<Link to={{
                    pathname: `directors/${this.props.director.id}`,
                }}>
                    <button>Edit</button>
                </Link>}
                <button onClick={this.deleteBtn}
                >Delete</button>
            </div>
        )
    }
}

// Creating directors items
function DirectorDivItem(props) {
    return (
        <div style={{
            display: "flex",
        }}>
            <div style={{
                width: "40%"
            }}
            > {props.property}</div> :
                <div
                style={{
                    width: "40%"
                }}
            >{props.value}</div>
        </div>
    )
}

export default connect(null,
    { deleteDirector, getData })
    (withRouter(DirectorDiv))