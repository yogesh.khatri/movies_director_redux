let initialState = {
    directors: []
}

const MoviesReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_DIRECTORS_STORE':
            {
                return {
                    ...state,
                    directors: action.directors
                }
            }
        default:
            return state;
    }
}
export default MoviesReducer