import { combineReducers } from 'redux';
import MoviesReducer from './reducer';

export default combineReducers({
    reducer: MoviesReducer
})