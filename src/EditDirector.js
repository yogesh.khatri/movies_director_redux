import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { updateDirectorById } from './actions'

class EditDirector extends Component {
    constructor(props) {
        super(props)
        this.director = {
            director: ''
        }
    }
    // Handling funciton to edit value
    handelChange = (e) => {
        this.director.director = e.target.value
    }

    // Handle Submit 
    handelSubmit = (e) => {
        e.preventDefault()
        console.log(this.props)
        console.log(this.director)
        this.props.updateDirectorById(this.props.params.id, this.director)
        this.props.history.push('/directors')

    }

    render() {
        // console.log(this.props.directors)
        return (
            <div>
                <div>
                    <form onSubmit={this.handelSubmit} onChange={this.handelChange}>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> Director Name</div>
                            <input name="director"
                                style={{ width: "40%" }}></input>
                        </div>
                        <input type="submit" value="submit" />
                    </form>
                </div>
            </div>
        )
    }
}

export default connect(null,
    { updateDirectorById })
    (withRouter(EditDirector))
