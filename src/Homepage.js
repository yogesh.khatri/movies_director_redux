import React, { Component } from 'react'
import { Link } from "react-router-dom"


class HomePage extends Component {
    render() {
        return (
            <div>
                <h1> Welcome to Movies Director</h1>
                <Link to="/movies">
                    <button>Movies</button>
                </Link>
                <Link to="/directors">
                    <button>Director</button>
                </Link>
            </div>
        )
    }
}

export default HomePage

