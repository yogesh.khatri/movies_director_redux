import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware,compose } from 'redux'
import { Provider } from 'react-redux'
import movieReducer from './reducers/index'
import createSagaMiddleware from 'redux-saga'
import mySaga from './sagas'
import Directors from './Directors';
import { BrowserRouter as Router, Route } from "react-router-dom";
import HomePage from './Homepage'
import AddDirector from './AddDirector'
import EditDirector from './EditDirector'

const sagaMiddleware = createSagaMiddleware();

//Defining store using reducer and sage middleware 
const store = createStore(movieReducer,
    compose(
        applyMiddleware(sagaMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
)

// running sage middleware
sagaMiddleware.run(mySaga)

// Rendereing DOM
ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Route path='/' exact component={HomePage} />
            <Route path='/directors' exact component={Directors} />
            <Route path='/addDirectors' exact component={AddDirector} />
            <Route path='/directors/:id' render={(props) => (
                <EditDirector params={props.match.params} />
            )} />
        </Router>
    </Provider>,
    document.getElementById('root'));