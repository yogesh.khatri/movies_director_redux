export const getData = () => (
    {
        type: 'GET_DIRECTORS'
    }
)
export const postDirector = (data) => (
    //  console.log(data+ "in action"),  
    {
        type: 'POST_DIRECTORS',
        data
    }
)
export const updateDirectorById = (id, director) => (
    {
        type: 'UPDATE_DIRECTOR',
        director,
        id
    }
)

export const deleteDirector = (id) => (
    // console.log(data + " in action"),
    {
        type: 'DELETE_DIRECTOR',
        id
    }
)