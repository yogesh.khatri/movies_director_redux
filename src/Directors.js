import React from 'react';
import { connect } from 'react-redux'
import { getData } from './actions'
import { Link } from 'react-router-dom';
import DirectorDiv from './DirectorDiv'

class Directors extends React.Component {

  // using lifecycle funciton to get date everytime when when component called
  componentDidMount() {
    this.props.getData()
  }

  render() {
    return (
      <div>
        <h1>All Directors</h1>
        <Link to='/addDirectors'>
          <button>Add Directors</button>
        </Link>
        <div style={{
          display: "flex",
          flexWrap: "wrap"
        }}>
          {
            this.props.directors.map((director) => {
              return <DirectorDiv
                director={director}
                key={director.id}
              />
            })
          }
        </div>
      </div>
    )
  }
}

// return object, derived from state, passed as props
const mapStateToProps = state => {
  return {
    directors: state.reducer.directors
  }
}

export default connect(
  mapStateToProps,
  { getData })(Directors)
