import React, { Component } from 'react'
import { connect } from 'react-redux'
import { postDirector } from './actions/index'

class AddDirector extends Component {
    constructor(props) {
        super(props);
        this.director = {
            director: ''
        }
    }

    handelSubmit = (e) => {
        e.preventDefault()
        this.props.postDirector(this.director)
        this.props.history.push('/directors')
    }

    onChange = (e) => {
        this.director.director = e.target.value
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handelSubmit} onChange={this.onChange}>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> Director</div>
                        <input name="director"
                            style={{ width: "40%" }}></input>
                    </div>
                    <input type="submit" value="submit" />

                </form>
            </div>
        )
    }
}

export default connect(null,
    { postDirector })(AddDirector)
