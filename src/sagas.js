import { put, takeLatest } from 'redux-saga/effects'

// fetch directors from api
function* fetchMovies() {
    let url = "http://localhost:4000/directors";
    const directors = yield fetch(url)
        .then((res) => {
            return res.json()
        })

    yield put({
        type: "GET_DIRECTORS_STORE",
        directors
    })
}

// Add directors to api
function* postDirector(data) {
    // console.log(data.data)
    let url = "http://localhost:4000/directors"
    yield fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data.data)
    })
        .then(res => {
            console.log(res)
        })
}

// update director director by id
function* updateDirectorById(data) {
    let url = "http://localhost:4000/directors/" + data.id
    yield fetch(url, {
        method: 'PUT',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data.director)
    })
        .then(res => {
            console.log(res)
        })
}

// delete director by id
function* deleteDirector(data) {
    console.log(data.id + " in sagas")
    let url = "http://localhost:4000/directors/" + data.id
    yield fetch(url, {
        method: 'DELETE'
    })
}

// Defining watcher functions
function* watcherFetchMovies() {
    yield takeLatest('GET_DIRECTORS', fetchMovies)
    yield takeLatest('POST_DIRECTORS', postDirector)
    yield takeLatest('UPDATE_DIRECTOR', updateDirectorById)
    yield takeLatest('DELETE_DIRECTOR', deleteDirector)
}

export default watcherFetchMovies